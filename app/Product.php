<?php

namespace App;

use App\Category;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    /**Uses de la Clase */
    use SoftDeletes;

    //definimos constantes para los posibles valores del atributo status
    const PRODUCTO_DISPONIBLE    = 'disponible';
    const PRODUCTO_NO_DISPONIBLE = 'no disponible';
    /**Atributtes de la Clase */
    protected $dates = ['deleted_at'];

    /* #region  Atributo Fillable */
    protected $fillable = [
        'name',
        'description',
        'quantity',
        'status',
        'image',
        'seller_id',
    ];

    protected $hidden = [
        'pivot'
    ];

    /* #endregion */

    /* #region  METODOS */

    public function transactions()
    {
        return $this->hasMany(Transaction::class);
    }

    /**
     * En el caso de que un producto es vendido por uno y solo un vendedor, entonces el nombre de la funcion es en singular
     */
    public function seller()
    {
        return $this->belongsTo(Seller::class);
    }

    public function categories()
    {
        return $this->belongsToMany(Category::class);
    }

    public function estaDisponible()
    {
        return $this->status == Product::PRODUCTO_DISPONIBLE;
    }

    /* #endregion */

}
