<?php

/**
 * Tiene todo el codigo necesario para construir las respuestas de nuestra API.
 * La palabra trait significa rasgo o caracteristica, haciendo alusion a el modo en que adaptamos la logica de negocio a nuestro caso de uso
 */

namespace App\Traits;

use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Model;

/**Los rasgos («traits» en inglés) son un mecanismo de reutilización de código en lenguajes de herencia simple, como PHP. El objetivo de un rasgo es el de reducir las limitaciones propias de la herencia simple permitiendo que los desarrolladores reutilicen a voluntad conjuntos de métodos sobre varias clases independientes y pertenecientes a clases jerárquicas distintas */
trait ApiResponser
{
	/**
	 * Este metodo se encarga de construir las respuestas satisfactorias
	 */
	private function successResponse($data, $code)
	{
		return response()->json($data, $code);
	}

	/**
	 * Metodo que muestra las respuestas de error
	 */
	protected function errorResponse($message, $code)
	{
		return response()->json(['error' => $message, 'code' => $code], $code);
	}

	/**
	 * Metodo que muestra todas las instancias de una respuesta, sean de la clase que sean
	 */
	protected function showAll(Collection $collection, $code = 200)
	{
		//vamos a decir que si la coleccion esta vacia, devolvemos el modo habitual de la informacion. Esto es una simple verificacion, porque el metodo first() no admite colecciones vacias
		if ($collection->isEmpty()) {
			return $this->successResponse(['data' => $collection], $code);
		}

		//Ahora si podemos utilizar dicho metodo. Aqui se utiliza el atributo transformer, sin saber si estamos mostrando usuarios, productos o algo mas. Por eso, es importante que este atributo se llame igual en todos los Model cuando estamos vinculando los Transformers con los datos puros:
		$transformer = $collection->first()->transformer;
		//Ahora usamos el metodo de la transformacion que definimos mas abajo en esta clase, que recibe dos parametros. Este resultado, se carga en la variable $collection, y es de tipo array
		$collection = $this->transformData($collection, $transformer);

		//si fuese el caso, se devuelve el array con los datos transformados
		return $this->successResponse([$collection], $code);
	}

	/**
	 * Metodo que muestra una instancia de una respuesta, sea de la clase que sea, y ademas tiene por codigo de respuesta HTTP el 200 ok por defecto como segundo parametro
	 */
	protected function showOne(Model $instance, $code = 200)
	{
		$transformer = $instance->transformer;
		$instance = $this->transformData($instance, $transformer);

		return $this->successResponse($instance, $code);
	}

	protected function showMessage($message, $code = 200)
	{
		return $this->successResponse(['data' => $message], $code);
	}

	/**
	 * La transformacion debe hacerse un array para que tenga sentido para nosotros. Porque sino es un objeto de la clase fractal que no tiene la estructura correcta y deseada
	 */
	protected function transformData($data, $transformer)
	{
		$transformation = fractal($data, new $transformer);

		return $transformation->toArray();
	}
}
