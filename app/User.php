<?php

namespace App;

use App\Transformers\UserTransformer;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Str;

class User extends Authenticatable
{
    use Notifiable, SoftDeletes;

    /* #region  ATRIBUTOS */
    const USUARIO_VERIFICADO    = '1';
    const USUARIO_NO_VERIFICADO = '0';

    const USUARIO_ADMINISTRADOR = 'true';
    const USUARIO_REGULAR       = 'false';
    //Este atributo es esencial cuando van a heredar este modelo otros modelos:
    protected $table = 'users';
    //Atributo para el borrado suave:
    protected $dates = ['deleted_at'];
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'verified',
        'verification_token',
        'admin',
    ];

    /**
     * Hidden: este atributo tiene como finalidad ocultar informacion en las respuestas de datos que hace el nackend al front. Laravel transforma este array en un objeto JSON, pero hay datos que no deben mostrarse por seguridad. Esos datos, van consignados en este array hidden.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token', //el token que se usa cuando el usuario tilda la opcion recordar
        'verification_token', //el codigo que se envia al correo del usuario para validarlo
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    //Para relacionar el transformer con su modelo, debemos colocar un atributo correspondiente en cada model, llamando al Transforme creado para cada Model en particular:
    public $transformer = UserTransformer::class;

    /* #endregion */

    /* #region  METODOS */
    public function esVerificado()
    {
        return $this->verified == User::USUARIO_VERIFICADO;
    }

    public function esAdministrador()
    {
        return $this->admin == User::USUARIO_ADMINISTRADOR;
    }

    public static function generarVerificationToken()
    {
        $random = Str::random(40);
        return $random;
    }

    /**
     *El mutador del atributo nombre, s encarga de guardar en la base de datos, los nombres de los users, todos en letras minusculas
     */
    public function setAttributeName($valor)
    {
        // con la parte izquierda de esta asignacion, accedemos al atributo name de esta clase, y con la parte derecha, le asignamos el valor del nombre pero todo en minusculas
        $this->attributes['name'] = strtolower($valor);
    }

    /**
     * El accesor del atributo name, no modifica el valor de la BD de name, sino que devuelve ese valor, pero modificado por una funcion de transformacion de datos.
     */
    public function getAttributeName($name)
    {

        //   return ucfirst($name); //hace que el primer valor del string sea mayuscula
        return ucwords($name); //hace que el primer valorde cada palabra del string sea mayuscula
    }

    /**
     *El mutador del atributo email, s encarga de guardar en la base de datos, los email de los users, todos en letras minusculas y con la estructura debida
     */
    public function setAttributeEmail($email)
    {
        // con la parte izquierda de esta asignacion, accedemos al atributo name de esta clase, y con la parte derecha, le asignamos el valor del nombre pero todo en minusculas
        $this->attributes['email'] = strtolower($email);
    }

    /* #endregion */
}
