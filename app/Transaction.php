<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Transaction extends Model
{
    /**Uses de la Clase */
    use SoftDeletes;
    /**Atributtes de la Clase */
    protected $dates = ['deleted_at'];

    /* #region  Atributo Fillable */
    protected $fillable = [
        'quantity',
        'buyer_id',
        'product_id',
    ];
    /* #endregion */

    /* #region  METODOS */

    public function buyer()
    {
        return $this->belongsTo(Buyer::class);
    }

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    /* #endregion */

}
