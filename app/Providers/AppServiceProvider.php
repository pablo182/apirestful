<?php

namespace App\Providers;

use App\Mail\UserCreated;
use App\Mail\UserMailChanged;
use App\Product;
use App\User;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //Resuelve el problema de exceso de caracteres para las migraciones hacia la base de datos:
        Schema::defaultStringLength(191);

        //Permite controlar que el estado del producto cambie cuando se agota
        Product::updated(function ($product) {
            if ($product->quantity == 0 && $product->estaDisponible()) {
                $product->status = Product::PRODUCTO_NO_DISPONIBLE;

                $product->save();
            }
        });

        //Cuando un usuario crea su email, se dispara este evento, es decir, al momento de created email. Aqui es dond ehacemos uso dela Mailable UserCreated
        User::created(function($user) {
            retry(5, function() use ($user) {
                Mail::to($user)->send(new UserCreated($user));
            }, 100);
        });

        /** Cuando el usuario modifica su email, se dispara este evento.
         *  El metodo isDirty() nos devuelve true cuando su parametro a cambiado, o sea, que el usuaro cambia el valor de dicho parametro, en este caso el email. Esto es asi, porque sino tal evento seria disparado por otras causas no deseadas.
         */
        User::updated(function($user) {
            if ($user->isDirty('email')) {
                retry(5, function() use ($user) {
                    Mail::to($user)->send(new UserMailChanged($user));
                }, 100);
            }
        });
    }
}
