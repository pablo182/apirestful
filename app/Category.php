<?php

namespace App;

use App\Product;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    /**Uses de la Clase */
    use SoftDeletes;
    /* #region  ATRIBUTOS */

    protected $dates = ['deleted_at'];
    /**
     * El atributo de asignacion masiva fillable se utiliza para definir datos de manera mas general, evitando escribir varias veces asignaciones de atributos individuales. Se listan en orden con tipo string, los nombre de los atributos en el vector
     */
    protected $fillable = ['name', 'description'];

    /**
     * El atributo pivote es el que se usa para vincular tablas N a N en la BD
     */
    protected $hidden = [
        'pivot',
    ];

    /* #endregion */

    /* #region  METODOS */

    /**
     * BelongToMany() es la funcion que permite implementar la realidad de N a N de tablas. Decimos que una categoria puede tener muchos productos.
     */
    public function products()
    {
        return $this->belongsToMany(Product::class);
    }
}
/* #endregion */
