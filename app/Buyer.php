<?php

namespace App;

use App\Transaction;
use App\Scopes\BuyerScope;

class Buyer extends User
{
    protected static function boot()
	{
        //Es importante isntanciar el constructor padre:
		parent::boot();

        //Le decimos que el scope que queremos usar es el definido para Buyer en App\Scopes
		static::addGlobalScope(new BuyerScope);
    }

    //decimos que el modelo Buyer tiene muchas Transacciones. En su constructor, reflejamos esto de la sig manera:
    public function transactions()
    {
        //Transaction::class accede al namespace de la clase y por lo tanto a sus atributos
        return $this->hasMany(Transaction::class);
    }

}
