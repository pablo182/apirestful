<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\ApiController;
use App\Mail\UserCreated;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class UserController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $usuarios = User::all();

        return $this->showAll($usuarios);
    }

    /**
     * Sirve para crear nuevas instancias de objetos. Se accede mediante la misma ruta de users, pero cambiando el metodo http a POST, es decir, cuando se necesita crear nuevos datos mediante un form
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     * La variable request guarda los datos de la respuesta del servidor web. Sirve para trabajar cone sos datos devueltos. Para crear datos nuevos u otra operacion
     */
    public function store(Request $request)
    {

        //Por medio de $reglas podemos darle parametros que se deben cumplir a los campos. esto forma un array que es una sola variable de verificacion
        $reglas = [
            'name'     => 'required',
            'email'    => 'required|email|unique:users',
            'password' => 'required|min:6|confirmed',
        ];
        //tomamos en una variable todos los campos de vueltos en la HTTP request
        $campos = $request->all();

        //el metodo por defecto validate() permite pasar el array de validacion anterior, y la variable request, para ser comparados y dar po bueno o no la verificacion de los campos y reglas que ponemos...
        $this->validate($request, $reglas);

        $campos['password']           = bcrypt($request->password);
        $campos['verified']           = User::USUARIO_NO_VERIFICADO;
        $campos['verification_token'] = User::generarVerificationToken();
        $campos['admin']              = User::USUARIO_REGULAR;
        //Aqui se crea un usuario nuevo. Esta forma se llama "asignacion masiva", porque asigna los datos de la variable campos a un usuario
        $usuario = User::create($campos);
        //LA respuesta es el usuario creado con codigo 201
        return $this->showOne($usuario, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {

        return $this->showOne($user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {

        //estas reglas son para que el update sea consistente:
        $reglas = [
            'email'    => 'email|unique:users,email,' . $user->id,
            'password' => 'min:6|confirmed',
            'admin'    => 'in:' . User::USUARIO_ADMINISTRADOR . ',' . User::USUARIO_REGULAR,
        ];

        $this->validate($request, $reglas);

        if ($request->has('name')) {
            $user->name = $request->name;
        }

        if ($request->has('email') && $user->email != $request->email) {
            $user->verified           = User::USUARIO_NO_VERIFICADO;
            $user->verification_token = User::generarVerificationToken();
            $user->email              = $request->email;
        }

        if ($request->has('password')) {
            $user->password = bcrypt($request->password);
        }

        if ($request->has('admin')) {
            if (!$user->esVerificado()) {
                return $this->errorResponse('Unicamente los usuarios verificados pueden cambiar su valor de administrador', 409);
            }

            //SIno (else), se hace esto:
            $user->admin = $request->admin;
        }

        //el metodo isDirty() se usa para verificar que al menos uno de los valores actuales haya cambiado. el codigo 422 significa peticion mal formada o entidad no procesable.
        if (!$user->isDirty()) {
            return $this->errorResponse('Se debe especificar al menos un valor diferente para actualizar', 422);
        }

        $user->save();

        return $this->showOne($user);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {

        $user->delete();
        return $this->showOne($user);
    }

    /**
     * Este metodo se encarga de la verificacion del email del usuario
     */
    public function verify($token)
    {
        //recibimos un token de verificacion, es decir un codigo alfanumerico, y buscamos un usuario con ese token asociado
        $user = User::where('verification_token', $token)->firstOrFail();

        $user->verified           = User::USUARIO_VERIFICADO;
        $user->verification_token = null;

        $user->save();

        return $this->showMessage('La cuenta ha sido verificada');
    }

    /**
     * Esta funcion reenvia el correo en caso de que no llegue y el usuario necesite verificar su email
     */
    public function resend(User $user)
    {
        if ($user->esVerificado()) {
            return $this->errorResponse('Este usuario ya ha sido verificado.', 409);
        }
        //Como no es necesario crear un nuevo archivo Mailable, utilizamos el mismo metodo de UserCreated(...)
        retry(5, function() use ($user) {
            Mail::to($user)->send(new UserCreated($user));
        }, 100);

        return $this->showMessage('El correo de verificación se ha reenviado');

    }

}
