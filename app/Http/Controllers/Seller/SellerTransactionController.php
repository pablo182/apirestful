<?php

namespace App\Http\Controllers\Seller;

use App\Http\Controllers\ApiController;
use App\Seller;

class SellerTransactionController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Seller $seller)
    {
        $transactions = $seller->products()
            ->whereHas('transactions')//sabemos que tienen transacciones
            ->with('transactions')//eager loading
            ->get()
            ->pluck('transactions')
            ->collapse();//siempre que se obtiene una lista de listas

        return $this->showAll($transactions);
    }

}
