<?php

namespace App\Http\Controllers\Seller;

use App\Http\Controllers\ApiController;
use App\Product;
use App\Seller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpKernel\Exception\HttpException;

class SellerProductController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Seller $seller)
    {
        $products = $seller->products;
        return $this->showAll($products);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, User $seller)
    {
        $rules = [
            'name'        => 'required',
            'description' => 'required',
            'quantity'    => 'required|integer|min:1', //entero, de valor minimo 1
            'image'       => 'required|image', //valor de tipo imagen
        ];

        $this->validate($request, $rules);
        //Se obtienen todos los datos:
        $data = $request->all();

        $data['status']    = Product::PRODUCTO_NO_DISPONIBLE;
        $data['image']     = $request->image->store('');
        $data['seller_id'] = $seller->id;
        //El producto nuevo se esta creando aqui:
        $product = Product::create($data);

        return $this->showOne($product, 201);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Seller  $seller
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Seller $seller, Product $product)
    {
        //en prefijo in se utiliza para decir que ese valor solo puede estar entre las alternativas separdas por coma que expresamente se le dicen:
        $rules = [
            'quantity' => 'integer|min:1',
            'status'   => 'in: ' . Product::PRODUCTO_DISPONIBLE . ',' . Product::PRODUCTO_NO_DISPONIBLE,
            'image'    => 'image',
        ];

        $this->validate($request, $rules);

        $this->verificarVendedor($seller, $product);

        $product->fill($request->only([
            'name',
            'description',
            'quantity',
        ]));

        if ($request->has('status')) {
            //Si solo se cambia el estado del producto solamente, se procede:
            $product->status = $request->status;

            //Si se pone a disponible el producto, sin tener asociada uan categoria, lanzamos un error, pues no puede haber productos sin categoria:
            if ($product->estaDisponible() && $product->categories()->count() == 0) {
                return $this->errorResponse('Un producto activo debe tener al menos una categoría', 409);
            }
        }

        //Actualizar imagen en la edicion de un producto:
        if ($request->hasFile('image')) {
            //Borramos la imagen antogua
            Storage::delete($product->image);

            //cargamos la imagen nueva de la modificacion
            $product->image = $request->image->store('');
        }

        if ($product->isClean()) {
            return $this->errorResponse('Se debe especificar al menos un valor diferente para actualizar', 422);
        }

        $product->save();

        return $this->showOne($product);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Seller  $seller
     * @return \Illuminate\Http\Response
     */
    public function destroy(Seller $seller, Product $product)
    {
        $this->verificarVendedor($seller, $product);
        //Antes de eliminar el producto, eliminamos tb la imagen asociada a el:
        Storage::delete($product->image);
        //se elimina el producto:
        $product->delete();

        return $this->showOne($product);
    }

    /**
     * Esta funcion es necesario por la regla de negocio que dice que solo puede modificr el producto el vendedor que sea poseedor del mismo. VAle decir, cada producto pertenece a un vendedor especifico, y solo este puede modificarlo o venderlo:
     */
    protected function verificarVendedor(Seller $seller, Product $product)
    {
        if ($seller->id != $product->seller_id) {
            throw new HttpException(422, 'El vendedor especificado no es el vendedor real del producto');
        }
    }
}
