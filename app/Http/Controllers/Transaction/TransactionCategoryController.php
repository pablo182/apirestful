<?php

namespace App\Http\Controllers\Transaction;

use App\Http\Controllers\ApiController;
use App\Transaction;

class TransactionCategoryController extends ApiController
{
    /**
     * A diferencia de los index normales, este resuelve por inyeccion implicita el id pasado en la URL, siendo esta la Transaction por la cual queresmos saber que categorias posee:
     *@param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function index(Transaction $transaction)
    {
        /**
         * Como en este modelo de datos, no existe relacion directa entre Transaction y Category, sino solo a traves de Product, debemos acceder a los productos de una transaccion para saber que categorias estan implicadas en esta request
         */
        $categories = $transaction->product->categories;

        return $this->showAll($categories);

    }
}
