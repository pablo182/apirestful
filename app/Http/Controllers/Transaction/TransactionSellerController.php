<?php

namespace App\Http\Controllers\Transaction;

use App\Http\Controllers\ApiController;
use App\Transaction;

class TransactionSellerController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Transaction $transaction)
    {
        //es importante usar seller como nombre de variable porque son los vendedores los que queremos obtener
        $seller = $transaction->product->seller;
        return $this->showOne($seller);
    }
}
