<?php

namespace App\Http\Controllers\Product;

use App\Http\Controllers\ApiController;
use App\Product;
use App\Transaction;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProductBuyerTransactionController extends ApiController
{
    /**
     * Controlador y metodo encargado de guardar una nueva operacion de compra de un Producto. Se requiere entonces un comprador que puede ser un User pues podria ser su primera compra, y una Transaction que guarda el hechod e la compra
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Product $product, User $buyer)
    {
        //la cantidad minima comprada debe ser 1
        $rules = [
            'quantity' => 'required|integer|min:1',
        ];

        $this->validate($request, $rules);

        //El comprador y el vendedor deben ser Users distintos
        if ($buyer->id == $product->seller_id) {
            return $this->errorResponse('El comprador debe ser diferente al vendedor', 409);
        }

        //comprador debe estar verificado
        if (!$buyer->esVerificado()) {
            return $this->errorResponse('El comprador debe ser un usuario verificado', 409);
        }
        //vendedor debe estar verificado
        if (!$product->seller->esVerificado()) {
            return $this->errorResponse('El vendedor debe ser un usuario verificado', 409);
        }

        if (!$product->estaDisponible()) {
            return $this->errorResponse('El producto para esta transacción no está disponible', 409);
        }

        //La cantidad del producto requerida no es suficiente:
        if ($product->quantity < $request->quantity) {
            return $this->errorResponse('El producto no tiene la cantidad disponible requerida para esta transacción', 409);
        }

        /**
         * Transacciones de BD: se utiliza para que las operaciones de compra u otras no se solapen, generando inconsistencias de datos. Asi, una transaccion se realiza toda entera, si y solo si, se hacen todos los pasos de la misma, y ademas, no se hace una sin que otra asociada a ella no se haya confirmado.
         * El parametro que recibe la transaction es una funcion que realiza la accion de guardar los datos en la BD
         */
        return DB::transaction(function () use ($request, $product, $buyer) {
            //se toma la cantidad requerida y se la resta de la cantidad actual del producto:
            $product->quantity -= $request->quantity;
            $product->save();

            //Se crea una instancia de Transaction con: Transaction::create([...]);
            $transaction = Transaction::create([
                'quantity'   => $request->quantity,
                'buyer_id'   => $buyer->id,
                'product_id' => $product->id,
            ]);

            return $this->showOne($transaction, 201);
        });
    }
}
