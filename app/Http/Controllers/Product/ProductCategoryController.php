<?php

namespace App\Http\Controllers\Product;

use App\Product;
use App\Category;
use App\Http\Controllers\ApiController;
use Symfony\Component\HttpFoundation\Request;

class ProductCategoryController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Product $product)
    {
        $categories = $product->categories;
        return $this->showAll($categories);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product, Category $category)
    {

        /**
         * Existen tres metodos posibles: sync, attach, syncWithoutDetaching. En este caso la relacion es Na N.
         * El metodo sync no sirve para agregar una ccategoria porque pisa la categoria/s que tenga el producto a modificar.
         *El metodo attach agrega la categoria, pero no diferencia si la que estamos agregando ya existe, pudiendo repetirse categorias en un mismo producto, es decir, con el mismo id de categoria, lo cual no es deseado.
         *El metodo syncWithoutDetaching agrega, no pisa y no repite los id de categorias.
        */
        $product->categories()->syncWithoutDetaching([$category->id]);

        return $this->showAll($product->categories);
    }

    /**
     * Lo que se elimina aqui es la relacion entre un producto especifico, y una categoria especifica de ese producto, y no una categoria general del ambito de estudio.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product, Category $category)
    {
        /**
         * El metodo categories() retorna la relacion de un producto con sus categorias. Luego se pide encontrar la categoria con el id especifico. En el caso de que ese producto no la tenga, entonces la operacion no es valida.
         */
        if (!$product->categories()->find($category->id)) {
            return $this->errorResponse('La categoría especificada no es una categoría de este producto', 404);
        }

        //El metodo detach() es para romper una ralacion entre dos modelos, pero solo en el sentido del id que se pasa como parametro al mnetodo
        $product->categories()->detach([$category->id]);

        return $this->showAll($product->categories);
    }

}
