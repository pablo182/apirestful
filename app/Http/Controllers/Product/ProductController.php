<?php

namespace App\Http\Controllers\Product;

use App\Product;
use App\Http\Controllers\ApiController;

class ProductController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::all();

        return $this->showAll($products);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        // $product->load('seller') nos permite obtener las relacion que existe en la base de datos relacional. En este caso, un producto tiene asociado una clave foranea de id de vendedor que lo vincula con uno y solo un vendedor. Si queremos mostrar datos de ese vendedor, al mostrar este producto, la funcion correcta es load() que recibe el nombre del modelo relacionado, y no el de la tabla: vale decir, seller y no sellers
        //return $this->showOne($product->load('seller'));
        // return Product::with('seller:id,name')->get();
        //La forma mas pura de devolver el objeto JSON es:
        return  $this->showOne($product);
    }
}
