<?php

namespace App\Http\Controllers\Buyer;

use App\Buyer;
use App\Http\Controllers\ApiController;

class BuyerController extends ApiController {
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        /***
         * Como Buyer hereda de User, no sirve preguntar por buyers en general. debemos decirle aquellos objetos que sean compradores, que tengan transacciones. el metodo has(...) recibe como parametro el nombre de la tabla o relacion que tenga ese modelo
         */
        $compradores = Buyer::has('transactions')->get();
        //aca usamos el metodo del Trait ApiResponse
        return $this->showAll($compradores);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Buyer $buyer) {

        //obtenemos los buyers (los users que tienen transactions, y le decimos, findOrFail por el id buscado)
        // $comprador = Buyer::has('transactions')->findOrFail($id);
        return $this->showOne($buyer);
    }

}
