<?php

namespace App\Http\Controllers\Buyer;

use App\Buyer;
use App\Http\Controllers\ApiController;

class BuyerSellerController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Buyer $buyer)
    {
        /**
         * Para llegar a los seller de cada buyer, se debe pasar priemro, por las transactions, preguntar por los products de estas, y a su vez de los products, quien es el seller de ese product.
         * La expresion with('product.seller') es resuelta por Laravel para saber que vendedor es de un determinado producto.
         * La expresion pluck('product.seller') es necesaria ya que los seller estan dentro de la estructura de datos de la coleccion, en el subindice product. Esto se expresa con la sintaxis product.seller para que Laravel vaya un nivel mas profundo en la estructura de datos y busque los seller.
         * La funcion unique(), despues de pluck(), es necesaria en este modelo de datos, ya que un mismo vendedor pudo haber vendido muchos productos, pero no nos interesa repetir el nombre del mismo vendedor muchas veces.
         * La funcion values() reorganiza los id uniques de los seller puesto que puede haber espacios vacios en la coleccion, que quedan por la eliminacion de seller repetidos, que no son deseados.
         */
        $sellers = $buyer->transactions()->with('product.seller')
            ->get()
            ->pluck('product.seller')
            ->unique('id')
            ->values();

        return $this->showAll($sellers);
    }
}
