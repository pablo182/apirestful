<?php

namespace App\Http\Controllers\Buyer;

use App\Buyer;
use App\Http\Controllers\ApiController;

class BuyerProductController extends ApiController
{
    /**
     * Los productos de un comprador o comprados por este.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Buyer $buyer)
    {
        /**
         * En este modelo de datos, una transaccion tiene un solo producto. Pero un comprador tiene una coleccion de transacciones. Esto hace que no se pueda acceder directamente a la instancia de transaccion, porque Laravel las transforma automaticamente en coleccion. No se puede hacer:
         * $buyer->transactions->product;
         * Para eso, se llama al query builder transactions() de Buyer, y no a la instancia directa Transaction.
         * Ademas se hace uso de eager loading de Eloquent (carga ansiosa).
         * El query builder permite utilizar los metodos de filtro sql como where.
         * En el caso de with(), indica el eager loading, que recibe la relacion que se desea acceder, y que esta vinculada con transaction.
         * Esto devuelve mucha informacion, que se puede ver con: dd($products). Dentro de esa collection, solo nos interesa el subindice products. Por eso, el metodo pluck() nos permite acceder a ese subindice, que contiene los datos de los productos de cada transaccion.
         */
        $products = $buyer->transactions()->with('product')
            ->get()
            ->pluck('product');

        return $this->showAll($products);
    }

}
