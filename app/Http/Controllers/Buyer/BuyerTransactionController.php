<?php

namespace App\Http\Controllers\Buyer;

use App\Buyer;
use App\Http\Controllers\ApiController;

class BuyerTransactionController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Buyer $buyer)
    {
        //En este caso la relacion es directa entre los Models asi que la consulta es directa
        $transactions = $buyer->transactions;

        //como un comprador puede tener mas de una transaccion, corresponde el metodo showAll()
        return $this->showAll($transactions);
    }

}
