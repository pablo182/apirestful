<?php
/**
 * Se hace un Controller intermedio que se llama ApiController, para manejar nuestra logica de controladores, sin extender directamente de la clase Controller de Laravel.
 * Cada controller creado en nuestra API extendera de este y no directamente de Controller, aunque recibe por herencia, todas las caracteristicas de Controller
 */
namespace App\Http\Controllers;

use App\Traits\ApiResponser;

class ApiController extends Controller {
 //Aqui decimos que usamos el Trait creado para este caso de uso. De esta manera, los controllers de nuestro campo de estudio heredaran los metodos traits definidos en ApiResponse
 use ApiResponser;
}
