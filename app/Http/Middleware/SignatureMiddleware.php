<?php

namespace App\Http\Middleware;

use Closure;

class SignatureMiddleware
{
    /**
     * Middleware de ejemplo, donde se toma el objeto HTTP response, y se le coloca una cabecera, vale decir, se modifica el header http, para ponerle en este caso el nombre de la aplcicacion correspondiente:
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $cabecera = 'X-cabecera')
    {
        //tomamos la respuesta de la consulta en una variable:
        $response = $next($request);

        //las respuestas http tienen atributos que modifican las partes http. uno de ellos es header. De alli le decismos set(...)
        $response->headers->set($cabecera, config('app.name'));
        return $response;
    }
}
