<?php

namespace App\Transformers;

use App\User;
use League\Fractal\TransformerAbstract;

class UserTransformer extends TransformerAbstract
{
  /**
   * List of resources to automatically include
   *
   * @var array
   */
  protected $defaultIncludes = [
    //
  ];

  /**
   * List of resources possible to include
   *
   * @var array
   */
  protected $availableIncludes = [
    //
  ];

  /**
   * A Fractal transformer.
   * Recibe como parametro objetos o arrays que se desean transformar
   * @return array
   */
  public function transform(User $user)
  {
    /**
     * se retorna un array que consta de juegos llave valor, donde la llave es el nuevo nombre que le damos al atributo, y el valor es el que realmiente vien de la base de datos.
     * Es importante realizar un cast explicito segun sea el dato que estamos trayendo:
     */
    return [
      //El id no se llama mas asi, sino que la aplicacion que lo consuma vera identificador como nombre
      'identificador' => (int) $user->id,
      'nombre' => (string) $user->name,
      'correo' => (string) $user->email,
      'verificado' => (int) $user->verified,
      //el valor booleano true o false, viene como string de la base de datos. ergo, se utiliza una comparacion con los tres === que significa: debe ser exactamente igual a...
      'esAdministrador' => ($user->admin === 'true'),
      'creadoEl' => (string) $user->created_at,
      'actualizadoEl' => (string) $user->updated_at,
      //debemos cerciorarnos de que un posible valor null devuelto no genere exceptions
      'eliminadoEl' => isset($user->deleted_at) ? (string) $user->deleted_at : null,
    ];
  }
}
