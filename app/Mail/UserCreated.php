<?php

namespace App\Mail;

use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class UserCreated extends Mailable
{
    use Queueable, SerializesModels;

    //creamos un atributo para obtener los datos cuando enviamos la response. Entonces Laravel construye esta clase ya con el usuario que estamos verificando
    public $user;
    /**
     * Create a new message instance.
     *Este es el constructor de la clase Mailable
     * @return void
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Build the message.
     *Este metodo se ejecuta automaticamente al enviar un mailable
     * @return $this
     */
    public function build()
    {
        //Usamos el metodo text para las purebas de API. Esta funcion nnos remite automaticamente a la carpeta view. Le estamos diciendo que ejecute el archivo blade que esta en view/emails y que se llama welcome. El metodo subject() agrega una descripcion del tema del email que se esta enviando
        return $this->text('emails.welcome')->subject('Por favor confirma tu correo electrónico');
        //El sig. codigo renderiza una vista html y viene por defecto al crear la clase:
        return $this->view('view.name');
    }
}
