<?php

use App\Category;
use App\Product;
use App\Transaction;
use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        //se desactivan momentaneamente las restricciones de integridad referencial para que no exista problemas al intentar borrar las tablas:
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        //Borramos los datos que pudieren existir en las tablas de los modelos:
        User::truncate();
        Category::truncate();
        Product::truncate();
        Transaction::truncate();
        //Como la tabla pivote no tiene un modelo, se accede a ella por medio de la clase DB que ya viene con Laravel
        DB::table('category_product')->truncate();

        /**
         * flushEventListeners() es un metodo que aplicado a los models, evita que se disparen eventos asoiados a los modelos.
         * En particular, es importante en el caso de los Mailable, pues si se ejecuta el seeder de la BD, al crearse 1000 usuarios, se estaria enviando 1000 emails de aviso porque el evento created de User esta activando tal comportamiento:
         */
        User::flushEventListeners();
        Category::flushEventListeners();
        Product::flushEventListeners();
        Transaction::flushEventListeners();

        /**
         * LAs siguientes lineas son para asignar cantidad de registros a las tablas:
         */
        $cantidadUsuarios      = 1000;
        $cantidadCategorias    = 30;
        $cantidadProductos     = 1000;
        $cantidadTransacciones = 1000;

        /**
         * La funcion factory() se denomina como un helper, y se encarga de crear los datos de los factories que creamos en ModelFactory.php
         * El orden de la creacion debe guardar un sentido relacional (en lo posible), dado que, no tiene mayor sentido crear primero una transaccion sin antes tener datos de productos en la BD:
         */
        factory(User::class, $cantidadUsuarios)->create();
        factory(Category::class, $cantidadCategorias)->create();
        //En el caso de los productos, cada uno debe estar asociado a una categoria. Para ello se usa la funcion each(...); esta significa, "para cada productor hacer...". Recibe un solo parametro que es en este caso una funcion anonima.
        factory(Product::class, $cantidadProductos)->create()->each(
            function ($producto) {
                //Obtenemos la lista de categorias de manera aleatoria, pero tomamos entre una y cinco categorias, por la regla de negocio que un producto puede tener mas de una categoria. Como estamos retornando mas de una categoria (una coleccion), utilizamos el metodo pluck() para indicar que solo queremos los id de esas categorias de la coleccion
                $categorias = Category::all()->random(mt_rand(1, 5))->pluck('id');
                //Parece que Laravel asume que por sel el primer parametro de la funcion factory del tipo Product model, la variable $producto de la funcion anonima es suceptible de llamar al metodo categories(). Asi, se les atachan los id de categorias obtenidos al azar mas arriba:
                $producto->categories()->attach($categorias);
            }
        );
        factory(Transaction::class, $cantidadTransacciones)->create();
    }
}
