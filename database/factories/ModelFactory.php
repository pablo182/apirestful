<?php

/**
 * @var \Illuminate\Database\Eloquent\Factory $factory
 */

use App\Category;
use App\Product;
use App\Seller;
use App\Transaction;
use App\User;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
 */

$factory->define(User::class, function (Faker $faker) {
  static $password;
  return [
    'name'               => $faker->name,
    'email'              => $faker->unique()->safeEmail,
    'email_verified_at'  => now(),
    'password'           => $password ?: $password = bcrypt('secret'),
    'remember_token'     => Str::random(10),
    //hacemos un random entre verificado o no verificado, y se lo guarda en una variable $verificado
    'verified'           => $verificado = $faker->randomElement([User::USUARIO_VERIFICADO, User::USUARIO_NO_VERIFICADO]),
    //Con la sintaxis siguiente, php hace una condicional IF (como sucede en otros lenguajes), en una sola linea. Si el valor de verificado es igual  a USUARIO_VERIFICADO (la constante), entonces el token de verificacion es null (no se necdesita), y sino, el valor del token es el resultado de la funcion generarVerificationToken() del modelo User.
    'verification_token' => $verificado == User::USUARIO_VERIFICADO ? null : User::generarVerificationToken(),
    //otro random para hacer aleatoriamente usuarios normales y admin
    'admin'              => $faker->randomElement([User::USUARIO_ADMINISTRADOR, User::USUARIO_REGULAR])
  ];
});

$factory->define(Category::class, function (Faker $faker) {
  return [
    //Fake tiene funciones: word se usa para generar palabras
    'name'        => $faker->word,
    //la descripcion sera un parrafo (texto) aleatorio
    'description' => $faker->paragraph(1)
  ];
});

$factory->define(Product::class, function (Faker $faker) {
  return [
    'name'        => $faker->word,
    'description' => $faker->paragraph(1),
    'quantity'    => $faker->numberBetween(1, 10),
    'status'      => $faker->randomElement([Product::PRODUCTO_DISPONIBLE, Product::PRODUCTO_NO_DISPONIBLE]),
    //Las imagenes las busca en public/img
    'image'       => $faker->randomElement(['1.jpg', '2.jpg', '3.jpg']),
    //el id del seller debe ser uno existente, luego, la funcion all() devuelve una coleccion de usuarios, de los cuales con random() se escoge uno al azar, y de ese, tomamos su id:
    'seller_id'   => User::all()->random()->id
    // 'seller_id' => User::inRandomOrder()->first()->id, //es otra forma
  ];
});

$factory->define(Transaction::class, function (Faker $faker) {
  //Obtenemos de manera aleatoria, un vendedor que tenga al menos un producto para vender:
  $vendedor = Seller::has('products')->get()->random();
  //Obtenemos de manera aleatoria, un usuario. No se saca de Buyer porque la regla de negocio dice que un usuario puede estar por comprar su primer producto, y por tanto se podria considerar como comprador tambien. Con la excepcion, de que el elegido, no sea el vendedor de la linea de codigo anterior:
  $comprador = User::all()->except($vendedor->id)->random();

  return [
    'quantity'   => $faker->numberBetween(1, 3),
    'buyer_id'   => $comprador->id,
    //accedemos a la lista de productos del vendedor arriba obtenido, de manera aleatoria, escogemos uno de ellos, y sacamos su id para esta fake transaction
    'product_id' => $vendedor->products->random()->id
  ];
});
